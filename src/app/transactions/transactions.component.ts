import { Component, ViewChild, OnInit } from '@angular/core';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

declare var require: any;
const data: any = require('../shared/data/company.json');

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})

export class TransactionsComponent implements OnInit {
	rows = [];
    temp = [];

    // Table Column Titles
    columns = [
        { name: 'Transaction ID' },
        { name: 'User ID' },
        { name: 'Membership ID' },
        { name: 'Amount' },
        { name: 'Renewal Date' },
        { name: 'Expiry Date' }
    ];
    @ViewChild(DatatableComponent) table: DatatableComponent;

	constructor() {
		this.temp = [...data];
		this.rows = data;
	}

	ngOnInit() {
		//this.users = JSON.parse(localStorage.getItem('currentUser'));
    	//console.log(this.users);
	}

   /*updateFilter(event) {
        const val = event.target.value.toLowerCase();

        // filter our data
        const temp = this.temp.filter(function (d) {
            return d.name.toLowerCase().indexOf(val) !== -1 || !val;
        });

        // update the rows
        this.rows = temp;
        // Whenever the filter changes, always go back to the first page
        this.table.offset = 0;
    }*/
}