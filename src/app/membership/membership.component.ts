import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';
import { MembershipService } from '../shared/data/membership.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DatatableComponent } from "@swimlane/ngx-datatable/release";

@Component({
  selector: 'app-membership',
  templateUrl: './membership.component.html',
  styleUrls: ['./membership.component.scss']
})
export class MembershipComponent implements OnInit {
	members = [];
	showMembership = false;
	memDetail;
	@ViewChild('f') floatingLabelForm: NgForm;
  @ViewChild('vform') validationForm: FormGroup;
  membershipForm: FormGroup;

	p: number = 1;
	totalList;
	previousPage: any;
	pageSize: number = 5;
	collectionSize;
  editMem = false;
  memId = 0;
  selectval = '';

  @ViewChild(DatatableComponent) table: DatatableComponent;

	constructor(public membershipservice: MembershipService, public router: Router) { }

	ngOnInit() {
		this.membershipForm = new FormGroup({
        'membershipName': new FormControl(null, [Validators.required]),
        'textArea': new FormControl(null, [Validators.required]),
        'price': new FormControl(null, [Validators.required]),
        'duration': new FormControl(null, [Validators.required]),
        'durationIn': new FormControl(null, [Validators.required])
    }, {updateOn: 'blur'});
		this.membershipservice.getMembership().then(data => {
			if(data['membership']){
        this.members = data['membership'];
				this.collectionSize = data['membership'].length;
				this.totalList = data['membership'].length;
			}
		},
		error => {
		});
	}

	 get f() { return this.membershipForm.controls; }

	 memToggle(){
      	this.showMembership = true;
  	}

    cancel(){
      window.location.reload();
    }

    addMembership(){
  		this.memDetail = {
  			memname: this.f.membershipName.value,
  			description: this.f.textArea.value,
  			duration: this.f.duration.value,
  			price: this.f.price.value,
        durationIn: this.f.durationIn.value
  		}
      if(!this.editMem){
    		this.membershipservice.addMembership(this.memDetail).then(data => {
    			if(data['status']){
        			this.showMembership = false;
              this.membershipForm.reset();
    				  //this.router.navigate(['/membership']);
    			}
    		});
      }
      else{
        this.memDetail = {
          memname: this.f.membershipName.value,
    			description: this.f.textArea.value,
    			duration: this.f.duration.value,
    			price: this.f.price.value,
          durationIn: this.f.durationIn.value,
          id: this.memId
        };
        this.membershipservice.updateMembership(this.memDetail).then(data => {
    			if(data['status']){
              console.log(data['membership']);
              this.membershipForm.reset();
        			this.showMembership = false;
              this.editMem = false;
    			}
    		});
      }
	  }

  editmembership(id){
    this.membershipservice.getMembershipById(id).then(data => {
			if(data['status'] == 'success'){
          this.membershipForm.controls['membershipName'].setValue(data['membership'][0]['plans']);
          this.membershipForm.controls['textArea'].setValue(data['membership'][0]['description']);
          this.membershipForm.controls['price'].setValue(data['membership'][0]['price']);
          this.membershipForm.controls['duration'].setValue(data['membership'][0]['terms']);
          this.membershipForm.controls['durationIn'].setValue(data['membership'][0]['duration']);
          this.selectval = data['membership'][0]['duration'];
          this.memId = data['membership'][0]['id'];
    			this.showMembership = true;
          this.editMem = true;
          console.log(this.selectval+','+data['membership'][0]['duration']);
				  //this.router.navigate(['/membership']);
			}
		});
  }

  deletemembership(id){
    this.membershipservice.deleteMembership(id).then(data => {
			if(data['status']){
			}
		});
  }

}
