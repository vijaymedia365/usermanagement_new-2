import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';
import { ArchwizardModule } from 'angular-archwizard';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ConfigurationsRoutingModule } from "./configurations-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { BuildChartComponent } from "./build-chart/build-chart.component";
import { TilesConfigComponent } from "./tiles-config/tiles-config.component";
import { SettingsComponent } from "./settings/settings.component";
import { ConfigurationsComponent } from "./configurations.component"

import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CustomFormsModule,
        ConfigurationsRoutingModule,
        Ng2SmartTableModule,
        NgxDatatableModule,
        NgbModule,
        ArchwizardModule,
        GooglePlaceModule
    ],
    declarations: [
        BuildChartComponent,
        TilesConfigComponent,
        SettingsComponent,
        ConfigurationsComponent
    ]
})
export class  ConfigurationsModule { }
