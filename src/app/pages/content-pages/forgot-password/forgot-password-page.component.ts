import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, Validators, NgForm } from '@angular/forms';

import { AuthenticationService } from '../../../shared/data/authentication.service';

@Component({
    selector: 'app-forgot-password-page',
    templateUrl: './forgot-password-page.component.html',
    styleUrls: ['./forgot-password-page.component.scss']
})

export class ForgotPasswordPageComponent {
    //@ViewChild('f') forogtPasswordForm: NgForm;
    forogtPasswordForm: FormGroup;
    error = false;
    success = false;
    failed = false;

    constructor(private router: Router,
        private route: ActivatedRoute,
        public authenticationService: AuthenticationService) { }

      ngOnInit(){
        this.forogtPasswordForm = new FormGroup({
            'inputEmail': new FormControl(null, [Validators.required])
        });
      }

    // On submit click, reset form fields
    onSubmit() {
        let passwordDetail = this.forogtPasswordForm.controls['inputEmail'].value;
        //console.log(passwordDetail);
        this.authenticationService.resetPassword(passwordDetail).then(data=>{
          if(data['status'] == 'success'){
            this.success = true;
            this.error = false;
            this.failed = false;
            console.log('password reset');
            this.forogtPasswordForm.reset();
          }
          else if(data['status'] == 'failed'){
            this.success = false;
            this.error = false;
            this.failed = true;
          }
          else if(data['status'] == 'error'){
            this.success = false;
            this.error = true;
            this.failed = false;
          }
        });
    }

    // On login link click
    onLogin() {
        this.router.navigate(['login'], { relativeTo: this.route.parent });
    }

    // On registration link click
    onRegister() {
        this.router.navigate(['register'], { relativeTo: this.route.parent });
    }
}
