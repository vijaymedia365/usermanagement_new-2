import { NgModule } from '@angular/core';
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CustomFormsModule } from 'ng2-validation';

import { Ng2SmartTableModule } from 'ng2-smart-table';
import { UsersRoutingModule } from "./users-routing.module";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AddUsersComponent } from "./add-users/add-users.component";
import { SubscribersComponent } from "./subscribers/subscribers.component";
import { UsersListComponent } from "./users-list/users-list.component";
import { EditUserComponent } from "./edit-user/edit-user.component";
import { MoveUsersComponent } from "./move-users/move-users.component";
import { ProfileComponent } from "./profile/profile.component";
import { NgxPaginationModule } from 'ngx-pagination';

import { GooglePlaceModule } from "ngx-google-places-autocomplete";

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        CustomFormsModule,
        UsersRoutingModule,
        Ng2SmartTableModule,
        NgbModule,
        NgxPaginationModule,
        GooglePlaceModule
    ],
    declarations: [
        AddUsersComponent,
        SubscribersComponent,
        UsersListComponent,
        EditUserComponent,
        MoveUsersComponent,
        ProfileComponent
    ]
})
export class UsersModule { }
