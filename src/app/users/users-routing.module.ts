import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddUsersComponent } from "./add-users/add-users.component";
import { SubscribersComponent } from "./subscribers/subscribers.component";
import { UsersListComponent } from "./users-list/users-list.component";
import { EditUserComponent } from "./edit-user/edit-user.component";
import { MoveUsersComponent } from "./move-users/move-users.component";
import { ProfileComponent } from "./profile/profile.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: 'add-user',
        component: AddUsersComponent,
        data: {
          title: 'Add User'
        }
      },
      {
        path: 'subscribers',
        component: SubscribersComponent,
        data: {
          title: 'Subscribers'
        }
      },
      {
        path: 'users',
        component: UsersListComponent,
        data: {
          title: 'Users'
        }
      },
      {
        path: 'edit-user',
        component: EditUserComponent,
        data: {
          title: 'Edit User'
        }
      },
      {
        path: 'move-users',
        component: MoveUsersComponent,
        data: {
          title: 'Move Users'
        }
      },
      {
        path: 'profile',
        component: ProfileComponent,
        data: {
          title: 'User Profile'
        }
      },
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UsersRoutingModule { }
