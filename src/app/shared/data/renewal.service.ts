import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable()
export class RenewalService {
    constructor(private http: HttpClient) { }

    public timestamp = new Date().getTime();

    getRenewal() {
        return new Promise(resolve => {
        this.http.get('https://click365.com.au/usermanagement/renewal.php?q='+this.timestamp+'&action=get')
            .subscribe(renewal => {
                console.log(renewal);               
                resolve(renewal);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }

    sendRenewalEmail(renewalId, userEmail, userName, plans, expire_date){
        let renewalDetails = {
            'renewalId' : renewalId, 
            'userEmail' : userEmail, 
            'userName' : userName, 
            'plans' : plans, 
            'expire_date' : expire_date
        }
        return new Promise(resolve => {
        this.http.post('https://click365.com.au/usermanagement/renewal.php?q='+this.timestamp+'&action=send',{renewalDetails})
            .subscribe(sentRenewal => {
                console.log(sentRenewal);               
                resolve(sentRenewal);
                }, err => {
               console.log("vbn"+ JSON.stringify(err));
            });
        });
    }
}